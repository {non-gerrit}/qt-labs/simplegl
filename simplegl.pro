#many of the qmake variables here are defined in .qmake.cache and hence
#this plugin must be built from within the Qt build tree or this profile
#file must be adjusted accordingly

TARGET = qscreensimplegl
include($$QT_SOURCE_TREE/src/plugins/qpluginbase.pri)

QTDIR_build:DESTDIR = $$QT_BUILD_TREE/plugins/gfxdrivers

target.path = $$[QT_INSTALL_PLUGINS]/gfxdrivers
INSTALLS += target

HEADERS = simpleglscreen.h simpleglwindowsurface.h
SOURCES = main.cpp simpleglscreen.cpp simpleglwindowsurface.cpp

QT += opengl

rasp-pi {
    message(using rasp-pi specific init)
    DEFINES += RASP_PI
}
