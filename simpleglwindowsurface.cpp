/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the plugins of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "simpleglwindowsurface.h"
#include "simpleglscreen.h"
#include <QDebug>

SimpleGLWindowSurface::SimpleGLWindowSurface(QWidget *w, SimpleGLScreen *s)
    : QWSGLWindowSurface(w), widget(w), screen(s)
{
    setSurfaceFlags(QWSWindowSurface::Opaque);
}

SimpleGLWindowSurface::SimpleGLWindowSurface()
    : QWSGLWindowSurface(), widget(NULL), screen(NULL)
{
    setSurfaceFlags(QWSWindowSurface::Opaque);
}

SimpleGLWindowSurface::~SimpleGLWindowSurface()
{
}

bool SimpleGLWindowSurface::isValid() const
{
    return (widget != 0);
}

QByteArray SimpleGLWindowSurface::permanentState() const
{
    // Nothing interesting to pass to the server just yet.
    return QByteArray();
}

void SimpleGLWindowSurface::setPermanentState(const QByteArray &state)
{
    Q_UNUSED(state);
}

void SimpleGLWindowSurface::flush
        (QWidget *widget, const QRegion &region, const QPoint &offset)
{
    // The GL paint engine is responsible for the swapBuffers() call.
    // If we were to call the base class's implementation of flush()
    // then it would fetch the image() and try manually blit it to
    // the screeen.
    Q_UNUSED(widget);
    Q_UNUSED(region);
    Q_UNUSED(offset);
}

QImage SimpleGLWindowSurface::image() const
{
    // No cross-platform way to fetch the window contents.
    return QImage(16, 16, screen->pixelFormat());
}

QPaintDevice *SimpleGLWindowSurface::paintDevice()
{
    return widget;
}
